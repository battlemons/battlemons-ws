var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const request = require('request');
var port = process.env.PORT || 8383;

app.get('/', function(req, res){
    res.send('<h1>Hello world</h1>');
});

io.on('connection', function(socket){
    var room;

    socket.on('joinGame', function(data) {
        room = data;

        io.of('/').adapter.clients([room], (err, clients) => {
            console.log(clients.length);

            if (clients.length < 2) {
                console.log('user ' + socket.id + " joined room " + room);
                socket.join(room);
                socket.emit('uid', socket.id);


                if (clients.length == 1) {
                    io.in(room).emit('startGame', 'start the game');
                }
            } else {
                socket.emit('fullRoom', "The room is already full");
            }
        });
    });

    socket.on('choosePokemon', function(data){
        io.in(room).emit('pokemonSelected', data);
    });

    socket.on('turn', function(data){
        if (data.length < 2 ) {
            // reguest for the other one first
            io.in(room).emit('waitingForMove', data);
        } else {

            // check who can go first (if the target of 0 is slower, it means turn 0 should go first)
            if (data[0].target.speed < data[1].target.speed) {
                console.log(data[0])
                getTurnResponse(room, data[0]);
                setTimeout(function () {
                    getTurnResponse(room, data[1]);
                }, 5000);
            } else {
                getTurnResponse(room, data[1]);
                setTimeout(function () {
                    getTurnResponse(room, data[0]);
                }, 5000);
            }


            io.in(room).emit('emptyActions', 'true');
        }
    });

    console.log('a user connected');
});

http.listen(port, function(){
    console.log('listening on *:' + port);
});

function getTurnResponse(room, turn){
    request.get('http://localhost:8082/api/turn/move/',
        {
            json: {
                target: {
                    name: turn.target.name,
                    type: {
                        id: turn.target.type.id,
                        description: turn.target.type.description,
                    },
                    hp: turn.target.hp,
                    speed: turn.target.speed
                },
                usedMove: {
                    description: turn.usedMove.description,
                    minDamage: turn.usedMove.minDamage,
                    maxDamage: turn.usedMove.maxDamage,
                    type: {
                        id: turn.usedMove.type.id,
                        description: turn.usedMove.type.description
                    }
                }
            }
        },
        (err, res, body) => {
            if (err) {
                return console.log(err);
            }

            var response = {
                id: turn.id,
                pokemon: body,
            }
            io.in(room).emit('turnResponse', response);
        });
}